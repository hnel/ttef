#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define bool int
#define true 1
#define false 0
#define STR_LENGTH 13

//#define SIZE 4
//we have to rename all functions to non-capital

void countfields(int** map, struct Stats* stats)
{
    int i,o,c;
    for(i = 0; i < SIZE; i++)
    {
        for(o = 0; o < SIZE; o++)
        {
            if(map[i][o] != 0)
            {
                (*stats).cm[i][o] = (*stats).cm[i][o] + 1;
            }
        }
    }
}

int ppower(int, int);
int stringToInt(char* string)   //the string needs to be terminated by the char '\n'
{
    //structure might appear a bit odd, but that is planned.
    int i,k,result = 0;
    for(i = 0; i < 12; i++)
    {
       if(string[i]=='\n'){break;} 
    }
    for(int k = 0; k < i; k++)
    {
        int digit = (char)string[k] - 48;
        int tp = ppower(10, i - k - 1);  
        printf("%d ",tp);
        result += digit * tp; 
    }
    return result;  
}

int ppower(int base, int exponent)
{
    if(base == 0){return 0;}
    if(exponent == 0){return 1;}
    int i;
    int result = base;

    for(i = 1; i < exponent; i++)
    {
	result = result * base;
    }
    return result;
}

int** fileToMap(char* path) //this will fail miserably when SIZE is set wrong
{
    int i,o,c = 0,k;
    int** map = generateMap();
    char* wholeNumber = malloc(10 * sizeof(char));
    
    FILE *file = fopen(path, "w");  //careful...
    if (file == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }

    for(i = 0; i < SIZE; i++)
    {
        for(o = 0; o < SIZE; o++)
        {   
            while(true)
            {
                
                char digit = getc(file);
                if(digit == EOF && i * o <= (SIZE - 1) * (SIZE - 1))
                {
                    printf("ERROR WHILE PARSING FILE TO MAP!! CHECK WHETHER SIZE IS SET CORRECTLY! Aborted... \n");
                }
                if(digit == ' '|| digit == '\n'); //this should jump overd double escapes and combin. of space + newline

                {
                    //inverse wholeNumber before proceeding
                    wholeNumber[c + 1] = '\n';
                    break;
                }

                if(digit > 47 && digit < 58)
                {
                    wholeNumber[c] = digit - 48;
                }
                
            }   
            map[i][o] = stringToInt(wholeNumber);
            free(wholeNumber);
        }
    }
}

void mapToFile(char* path, int** map)
{
    int i,o,c,k;
    FILE* file = fopen(path,"w");
    for(i = 0; i < SIZE; i++)
    {
        for(o = 0; o < SIZE; o++)
        {
            fprintf(file, "%d",map[i][o]);
            if(o < (SIZE - 1)){fprintf(file, " ");}
            else{fprintf(file, "\n");}
        }
    }
   //fprintf(file, 'EOF');} this cant be done, and is not neccessary?
}

int** tileFingerprint(int** map, int tile,struct Stats* stats)  //tile is the exponent to the tile (base 2)
{   int i,o;
    for(i = 0; i < SIZE; i++){ for(o = 0; o < SIZE; o++)
    {
        if(map[i][o] == tile){stats -> tf[i][o] += 1;}
    }}
}

void countDirections(int m, struct Stats* stats)
{
    stats -> choiceCount[m] += 1;  //double check this. 
}

int maxtile(int** map)
{
    int i,o,max = 0;    //fuck performance
    for(i = 0; i < SIZE; i++){for(o = 0; o < SIZE; o++)
    {
        if(map[i][o] > max){max = map[i][o];}            
    }}
    return max;
}


void directionLog(int modcode, struct Stats* stats)
{
    (*stats).choices[(*stats).i++] = modcode + 1;
    //stats -> choices[stats -> i++] = modcode + 1;
}
