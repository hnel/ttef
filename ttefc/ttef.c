#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define bool int
#define true 1
#define false 0

int MOVES = 4;

int SIZE = 4;

void setmoves(int* SIZE, int value)
{
    *SIZE = value;
}

//TTEF Go fuck yourself. Compile it yourself

struct Stats    //this is the structure later containing Data.
{
    int score;
    int draws;
    int bestTile;
    int Time;
    int zeroes;
    int** cm;
    int* choices;
    int* scoreTrace;
    int** tf;
    int i;
    int* choiceCount;
}Stats;

void INIT() //initialize the randum number generator. Time will be used as seed	
{
    srand(time(NULL));
}


int rnd(int range)  //function that returns a random number. In c the standard rand() func returns a number between 0 and 32768. We use modulo to yield a specific range
{   
    if(range == 1){ return 0;}
    int k;
    k = rand();
    return  k % (range + 1);
}

int** generateMap()     //Initializes a new 2d arrar, that in this program is genuinely called 'map'
{
    int i;
    int* values = calloc(SIZE*SIZE, sizeof(int));   //memory allocation for list
    int** rows = malloc(SIZE*sizeof(int*));         //"intercept" pointer for row adress
    for (i = 0; i < SIZE; ++i)
    {
        rows[i] = values + i*SIZE;
    }
    return rows;
}


void setStatsZero(struct Stats* stats)  //self explainig
{
	stats -> score = 0;
	stats -> draws = 0;
	stats -> bestTile = 0;
	stats -> Time = 0;
	stats -> zeroes = 0;
        (*stats).cm = generateMap();
        (*stats).tf = generateMap();
        (*stats).choiceCount = calloc(MOVES, sizeof(int));
        (*stats).choices = calloc(100000, sizeof(int));
        stats -> i = 0;
}

int countZeroes2d(int** map)    //counts zeros in map, linear search aswell
{
	int counter = 0, x, y;

	for(x = 0; x < SIZE; x++)
    {
		for(y = 0; y < SIZE; y++)
		{
			if((map)[x][y] == 0){counter++;}
		}
	}
	return counter;
}

void rmzeros(int*** map, int modcode)
{
    if(modcode == 0)
    {int outer = 0;
        for(outer = 0; outer < SIZE; outer++)
        {
            int read = 0, write = 0;
            for(read = 0; read < SIZE; read++)
            {
                if((*map)[outer][read] != 0)
                {
                    (*map)[outer][write] = (*map)[outer][read];
                    if(read > write++){(*map)[outer][read] = 0;}
                }
            }
        }
    }
    if(modcode == 3)
    {int outer = 0;
        for(outer = 0; outer < SIZE; outer++)
        {
            int read = 0, write = 0;
            for(read = 0; read < SIZE; read++)
            {
                if((*map)[read][outer] != 0)
                {
                    (*map)[write][outer] = (*map)[read][outer];
                    if(read > write++){(*map)[read][outer] = 0;}
                }
            }
        }
    }
    if(modcode == 2)
    {int outer = 0;
        for(outer = 0; outer < SIZE; outer++)
        {
            int read = 0, write = 3;
            for(read = 0; read < SIZE; read++)
            {
                if((*map)[outer][SIZE - read - 1] != 0)
                {
                    (*map)[outer][write] = (*map)[outer][SIZE - read - 1];
                    if((SIZE - read - 1) < write--){(*map)[outer][SIZE - read - 1] = 0;}
                }
            }
        }
    }
    if(modcode == 1)
    {int outer = 0;
        for(outer = 0; outer < SIZE; outer++)
        {
            int read = 0, write = 3;
            for(read = 0; read < SIZE; read++)
            {
                if((*map)[SIZE - read - 1][outer] != 0)
                {
                    (*map)[write][outer] = (*map)[SIZE - read - 1][outer];
                    if((SIZE - read - 1) < write--){(*map)[SIZE - read - 1][outer] = 0;}
                }
            }
        }
    }
}

bool checkZeroes(int** map) //Checks whether a zero is present in the map
{
	int x,y;
	for(x = 0; x < SIZE; x++)
	{
		for(y = 0; y < SIZE; y++)
		{
			if(map[x][y] == 0){return true;}
		}
	}
	return false;
}


int power(int base, int exponent);


int mergeTogether(int*** map, int modcode, struct Stats* stats)
/***************************************************************************
 *  This deserves its own explaination.
 *
 *  This function takes a map, a modcode and the statistics, and perform a
 *   merge. This means: Input: eg.
 *      
 *      [1,0,0,3,3,0,4] is transformed to [1,5,0,0,0,0,0]
 *   
 *   Equal numbers other than zero are pushed into a certain direction,
 *   based on the modcode thats being passed to the function. Basically the
 *   function doesnt need a return value, because we are passing pointers to
 *   the function. It actually returns the score gained by the move
 *   
 *   Modcodes as always. 3 -> up; 0 -> left; 1 -> right; 2 -> down
 **************************************************************************/
 
{
    int inner, outer, i, c,k, thisMoveScore = 0;
    if(modcode == 0)
    {rmzeros(map, modcode);
	for(outer = 0; outer < SIZE; outer++)
	{
	    for(i = 0; i < SIZE; i++)
	    {	
		for(inner = 0; inner < SIZE - 1; inner++)
		{
		    if((*map)[outer][inner] == (*map)[outer][inner + 1] && ((*map)[outer][inner] != 0)&&((*map)[outer][inner + 1] != 0))
		    {
			stats -> score += 2 * power((*map)[outer][inner], 2);
			thisMoveScore += 2 * power((*map)[outer][inner],2);
			(*map)[outer][inner + 1] = 0;
			(*map)[outer][inner] = (*map)[outer][inner] + 1;
			break;
		    }
		}
		rmzeros(map,modcode);
	    }
	}
     }
    if(modcode == 3)
    {rmzeros(map, modcode);
	 for(outer = 0; outer < SIZE; outer++)
	    {
		for(i = 0; i < SIZE; i++)
		{	
		    for(inner = 0; inner < SIZE - 1; inner++)
		    {
			if((*map)[inner][outer] == (*map)[inner + 1][outer] && (*map)[inner][outer] != 0)
			{
			    stats -> score += 2 * power((*map)[inner][outer], 2);
			    thisMoveScore += 2 * power((*map)[inner][outer], 2);
			    (*map)[inner + 1][outer] = 0;
			    (*map)[inner][outer] = (*map)[inner][outer] + 1;
			    break;
			}
		    }
		    rmzeros(map,modcode);
		}
	    }
	}

    if(modcode == 2)
    {
    	rmzeros(map,modcode);
	for(outer = 0; outer < SIZE; outer++)
	{
	    for(i = 0; i < SIZE; i++)
	    {	
		for(inner = 0; inner < SIZE - 1; inner++)
		{
		    if((*map)[outer][SIZE - 1 - inner] == (*map)[outer][SIZE - 2 - inner] && ((*map)[outer][SIZE - 1 - inner] != 0))
		    {
			stats -> score += 2 * power((*map)[outer][SIZE - 1 - inner], 2);
			thisMoveScore += 2 * power((*map)[outer][SIZE - 1 - inner], 2);
			(*map)[outer][SIZE - 2 - inner] = 0;
			(*map)[outer][SIZE - 1 - inner]++;	
			break;
		    }
		}
		rmzeros(map,modcode);
	    }
	}return thisMoveScore;
    }
    if(modcode == 1)
    {	rmzeros(map,modcode);
	 for(outer = 0; outer < SIZE; outer++)
	    {
		for(i = 0; i < SIZE; i++)
		{	
		    for(inner = 0; inner < SIZE - 1; inner++)
		    {
			if((*map)[SIZE - 1 - inner][outer] == (*map)[SIZE - 2 - inner][outer] && (*map)[SIZE - 1 - inner][outer] != 0)
			{
			    stats -> score += 2 * power((*map)[SIZE - 1 - inner][outer], 2);
			    thisMoveScore += 2 * power((*map)[SIZE - 1 - inner][outer], 2);
			    (*map)[SIZE - 2 - inner][outer] = 0;
			    (*map)[SIZE - 1 - inner][outer]++;
			    break;
			}
		    }
		    rmzeros(map,modcode);
		}
	    }
	}
	stats -> draws += 1;
}   

int power(int exponent, int base)   //base to the power of exponent
{
	if(base == 0){return 0;}
	if(exponent == 0){return 0;}
	int i;
	int result = base;
	
	for(i = 1; i < exponent; i++)
	{
		result = result * base;
	}
	return result;
}

void printMap(int** map, bool powerr)   //easy printmap. Prints map with or without power
{
    if(powerr == true){
    	int x,y;
    	for(x = 0; x < SIZE; x++)
    	{
			for(y = 0; y < SIZE; y++)
			{
	    		printf("[ %d \t]", power(map[x][y], 2));
			}
			printf("\n");
		}
	}else{
		int x,y;
		for(x = 0; x < SIZE; x++)
		{
			for(y = 0; y < SIZE; y++)
				{
			    printf("[ %d \t]", map[x][y]);
				}
			printf("\n");
		} 
	}
}


int intLenght(int value)    //gets the length of a number
{
	int lenght = 1;

	while(value > 10 || value == 10)
	{
		lenght++;
		value = value / 10;
	}

	return lenght;
}


void prettyPrintMap(int** map, struct Stats* stats) //print the map beautifully. Written by B.
{
	int lenLongestNumber = 0, lenDifference, x, y, i;

	for(x = 0; x < SIZE; x++)
	{
		for(y = 0; y < SIZE; y++)
		{
			if(intLenght(power(map[x][y], 2)) > (lenLongestNumber))
			{
				lenLongestNumber = intLenght(power(map[x][y], 2));
			}
		}
	}

	for(x = 0; x < SIZE; x++)
	{
		for(y = 0; y < SIZE; y++)
		{
			printf("|");

			for(i = 0; i < (lenLongestNumber - intLenght(power(map[x][y], 2))); i++)
			{
				printf(" ");
			}
			if(power(map[x][y],2))
			{
				printf("%d", power(map[x][y], 2));
			}else
			{
				printf(" ");
			}
		}
		printf("|\n");
	}
	printf("Moves: %d\n", (stats -> draws));
	printf("Score: %d\n", (stats -> score));
}


void manualPutTwo(int*** map, int x, int y)
{
	if((*map)[x][y] == 0){ (*map)[x][y] = 1;}
}

void putRandomTwo(int*** map)
{
	int randomX = rnd(SIZE - 1);
	int randomY = rnd(SIZE - 1);
	if((*map)[randomX][randomY] == 0){(*map)[randomX][randomY] = 1;}
	else{putRandomTwo(map);}
}

int bestTile(int** map)
{
	int x,y,bestTile;

	for(x = 0; x < SIZE; x++)
    {
		for(y = 0; y < SIZE; y++)
		{
			if(bestTile < map[x][y]){bestTile = map[x][y];}
		}
	}
	return power(2, bestTile);	
}

struct Stats playTTEF()
{
	struct Stats stats;
	stats.score = 0;

	int**map = generateMap();
	putRandomTwo(&map);
	putRandomTwo(&map);

	while(checkZeroes(map) == true)
	{	
		char input = getchar();
			
		system("clear");
		prettyPrintMap(map, &stats);
		printf("SCORE: %d \t LAST MOVE: %c \n",stats.score, input);
		if(input == ':')
		{
		    char action = getchar();
		    if(action == 'q'){exit(0);}
		}			
		if(input == 'a'){mergeTogether(&map, 0, &stats);putRandomTwo(&map);}
		if(input == 's'){mergeTogether(&map, 1, &stats);putRandomTwo(&map);}
		if(input == 'd'){mergeTogether(&map, 2, &stats);putRandomTwo(&map);}
		if(input == 'w'){mergeTogether(&map, 3, &stats);putRandomTwo(&map);}
	}

	system("clear");
	prettyPrintMap(map, &stats);
	printf("You lost due to no zeroes!");

	return stats;
}


float shitmark(int runs)    //shitmark GIANT MEMORY LEAK?
{
	clock_t begin = clock();
	int bestScore = 0;

	printf("SHITMARK STARTED WITH %d RUNS...\n",runs);
	for(int i = 0; i < runs; i++)
	{	
		int** map = generateMap();
		struct Stats stats;
		stats.score = 0;
			
			while(checkZeroes(map) == true)
			{
			    mergeTogether(&map, rnd(3),&stats);
			    putRandomTwo(&map);
			}
		if(stats.score > bestScore){bestScore = stats.score;}
	}
	printf("READY... \n");
	printf("\n");
	printf("\n");
	printf("HIGHSCORE: %d \n",bestScore);

	clock_t end = clock();
	double timeSpent = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("TOOK: %f \n",timeSpent);
	return timeSpent;
}


void put(int*** map, int v, int x, int y)
{
	(*map)[x][y] = v;
}

int** cpmap(int** originalMap)
{
    int x,y,z;
    int** copy = generateMap();
    for(x = 0; x < SIZE; x++)
    {
        for(y = 0; y < SIZE; y++)
	{
	    copy[x][y] = originalMap[x][y];
	}
    }return copy;
}

/*int main()
{
    playTTEF();
    return 0;
}*/
