//#include <stdlib.h>
//#include <stdio.h>
//#include "ttef.h"
#include "algs.c"
#ifndef _ALGS_H_
#define _ALGS_H_
#define bool int

float mean(float* values, int lenght);

void randomMoveAlgorithm(struct Stats* stats, bool print);

int evaluateOutcome(struct Stats* stats, int scoreDifference);

int***** lookFiveMovesAheadScore(int** map, struct Stats* stats);

int*** lookThreeMovesAheadScore(int** map, struct Stats* stats);

int bestPotEasy3Algorithm(struct Stats* stats, bool print);

int bestPotEasy5Algorithm(struct Stats*,bool);

int bestPotMove5(int** map);
