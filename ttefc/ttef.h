//this is the official ttef header file. All methods that
//need to be called when
#include "ttef.c"

#ifndef _TTEF_H_
#define _TTEF_H_
#define bool int
//#define move int;
//#define false 1
//#define true 1

void INIT();

int MOVES;

int rnd(int range);

void setStatsZero(struct Stats* stats);

int** generateMap();

int findFirstZero(int** map, int row, int modcode);

int countZeros2d(int** map);

void rmzeros(int *** map, int modcode);

bool checkZeroes(int** map); //this needs to be renamed instantly.

int power(int base, int exponent); //rename this!!

int mergeTogether(int*** map, int modcode, struct Stats* stats);

void printMap(int** map, bool powerr);

void prettyPrintMap(int** map, struct Stats* stats);

void putRandomTwo(int*** map);

int bestTile(int** map);

float shitmark(int runs);

int** cpmap(int** originalMap);


extern struct Stats globalStats;
/*	Now when defining a new struct you have to call it globalStats
*/


#endif


//This is it man. There are no other important fuctions that would
//need to be called from extern
