#include <stdio.h>
#include <stdlib.h>
#include "ttef.h"
#include "data.h"
#define MOVES 4
//#define SIZE 4
//If you want to change the field size, you have to change the 
//global define. This has to be done in ttef.c!


float mean(float* values, int lenght)
{
	int i; 
	float allValues;

	for(i = 0; i < lenght; i++)
	{
		allValues += values[i];
	}

	return (allValues / (float)lenght);
}

void randomMoveAlgorithm(struct Stats* stats, bool print)
{
	clock_t begin = clock();
	int** map = generateMap();

	putRandomTwo(&map);
	putRandomTwo(&map);

	while(checkZeroes(map) == true)
	{
		if(print == true){prettyPrintMap(map, stats);}
                countfields(map, stats);
		mergeTogether(&map, rnd(3), stats);
                putRandomTwo(&map);
	}
	
	if(print == true){prettyPrintMap(map, stats);}

	clock_t end = clock();
	double timeSpent = (double)(end - begin) / CLOCKS_PER_SEC;
    prettyPrintMap(map, stats);
    printf("That took %f!\n", timeSpent);
}


int evaluateOutcome(struct Stats* stats, int scoreDifference)
{
	return (stats -> zeroes) * scoreDifference;
}


int***** lookFiveMovesAheadScore(int** map, struct Stats* stats)
{
	int *****options;
	int firstMove, secondMove, thirdMove, fourthMove, fifthMove;
	int** mapCopy = generateMap();
	int scoreBefore = (stats -> score);
	int drawsBefore = (stats -> draws);
	int i, j, k, o;

	options = malloc(MOVES * sizeof(int ****));
    for(i = 0; i < MOVES; ++i)
    {
        options[i] = malloc(MOVES * sizeof(int ***));
        for(j = 0; j < MOVES; ++j)
        {
            options[i][j] = malloc(MOVES * sizeof(int **));
			for(k = 0; k < MOVES; ++k)
			{
				options[i][j][k] = malloc(MOVES * sizeof(int *));
				for(o = 0; o < MOVES; ++o)
				{
					options[i][j][k][o] = malloc(MOVES);
				}
			}
        }
    }

	for(firstMove = 0; firstMove < MOVES; firstMove++)
	{
		for(secondMove = 0; secondMove < MOVES; secondMove++)
		{
			for(thirdMove = 0; thirdMove < MOVES; thirdMove++)
			{
				for(fourthMove = 0; fourthMove < MOVES; fourthMove++)
				{
					for(fifthMove = 0; fifthMove < MOVES; fifthMove++)
					{
						mapCopy = map;
						mergeTogether(&mapCopy, firstMove, stats);
						mergeTogether(&mapCopy, secondMove, stats);
						mergeTogether(&mapCopy, thirdMove, stats);
						mergeTogether(&mapCopy, fourthMove, stats);
						mergeTogether(&mapCopy, fifthMove, stats);

						stats -> zeroes = countZeroes2d(mapCopy);
						int scoreDifference = (stats -> score) - scoreBefore;
						options[firstMove][secondMove][thirdMove][fourthMove][fifthMove] = evaluateOutcome(stats, scoreDifference);
					}
				}
			}
		}
	}

	//(stats -> score) = scoreBefore;
	(stats -> draws) = drawsBefore;

	return options;
}


int*** lookThreeMovesAheadScore(int** map, struct Stats* stats)    //rewrite this piece of shit! you dont need the struct. mt returns the score! Look into ttef137gg!
{
	int ***options;
	int firstMove, secondMove, thirdMove;
	int** mapCopy = generateMap();
	int scoreBefore = (stats -> score);
	int drawsBefore = (stats -> draws);
	int i, j;

	options = malloc(MOVES * sizeof(int **));
    for (i = 0; i < MOVES; ++i)
    {
        options[i] = malloc(MOVES * sizeof(int *));
        for (j = 0; j < MOVES; ++j)
        {
            options[i][j] = malloc(MOVES);
        }
    }

	for(firstMove = 0; firstMove < MOVES; firstMove++)
	{
		for(secondMove = 0; secondMove < MOVES; secondMove++)
		{
			for(thirdMove = 0; thirdMove < MOVES; thirdMove++)
			{
				mapCopy = map;
				mergeTogether(&mapCopy, firstMove, stats);
				mergeTogether(&mapCopy, secondMove, stats);
				mergeTogether(&mapCopy, thirdMove, stats);

				stats -> zeroes = countZeroes2d(mapCopy);
				int scoreDifference = (stats -> score) - scoreBefore;
				options[firstMove][secondMove][thirdMove] = evaluateOutcome(stats, scoreDifference);
			}
		}
	}

	//(stats -> score) = scoreBefore;
	(stats -> draws) = drawsBefore;

	return options;
}

int bestPotMove3(int** map, struct Stats* stats)
{

    int ***options = lookThreeMovesAheadScore(map, stats);
    int localBest = 0;
    int i,o,c,move;
    int* bestMove = malloc(MOVES*sizeof(int));

    for(int i = 0; i < MOVES; i++)
    {
		int m = 0;
		for(o = 0; o < MOVES; o++){for(c = 0; c < MOVES; c++)
		{
	    	m += options[i][o][c];
		}
		bestMove[i] = m;
    }
	}

    for(i = 0; i < MOVES; i++)
    {
		if(bestMove[i] > localBest){localBest = bestMove[i]; move = i;}
    }

    return move;

}

int bestPotEasy3Algorithm(struct Stats* stats, bool print)
{
	int initialStats = stats -> score;
	int** map = generateMap();
	putRandomTwo(&map);
	putRandomTwo(&map);

    clock_t begin = clock();

    while(checkZeroes(map) == true)
    {
		int bestMove = bestPotMove3(map, stats);
		if(print == true){prettyPrintMap(map, stats); }
                //put this into an analytics block
                countfields(map, stats);
                 
                countDirections(bestMove, stats);

                tileFingerprint(map, maxtile(map), stats);
                directionLog(bestMove, stats);
                
                mergeTogether(&map, bestMove, stats);
		putRandomTwo(&map);

                printf("Next move: %d \n",bestMove);
    }

    clock_t end = clock();
    double timeSpent = (double)(end - begin) / CLOCKS_PER_SEC;
    system("clear");prettyPrintMap(map, stats);
    printf("That took %f!\n", timeSpent);
    return stats -> score - initialStats;
}


int bestPotMove5(int** map, struct Stats* stats)
{

    int *****options = lookFiveMovesAheadScore(map, stats);
    int localBest = 0;
    int i,o,c,d,p,move;
    int* bestMove = malloc(MOVES*sizeof(int));

    for(int i = 0; i < MOVES; i++)
    {
		int m = 0;
		for(o = 0; o < MOVES; o++)
		{
			for(c = 0; c < MOVES; c++)
			{
	    		for(d = 0; d < MOVES; d++)
				{
					for(p = 0; p < MOVES; p++)
					{
						m += options[i][o][c][d][p];
					}
					bestMove[i] = m;
				}
			}
    	}
	}

    for(i = 0; i < MOVES; i++)
    {
		if(bestMove[i] > localBest){localBest = bestMove[i]; move = i;}
    }
	
    return move;
}


int bestPotEasy5Algorithm(struct Stats* stats, bool print)
{
	int** map = generateMap();
	putRandomTwo(&map);
	putRandomTwo(&map);

    clock_t begin = clock();

    while(checkZeroes(map) == true)
    {
		int bestMove = bestPotMove5(map, stats);
		if(print == true){system("clear");prettyPrintMap(map, stats);printMap(stats -> cm, false);}
		
                countfields(map,stats);
                countDirections(bestMove, stats);
                directionLog(bestMove, stats);

                mergeTogether(&map, bestMove, stats);
		putRandomTwo(&map);
    }

    clock_t end = clock();
    double timeSpent = (double)(end - begin) / CLOCKS_PER_SEC;
    if(print == true){system("clear");prettyPrintMap(map, stats);}
    if(print == true){printf("That took %f!\n", timeSpent);}

    return stats -> score;
}

float intmean(int* list, int length)
{
    int i, o = 0;
    float avrg = 0;
    for(i = 0; i < length; i++)
    {
	o += list[i];
    }
    avrg =(float)o / length;
    return avrg;	//theoretically this is not neccessary;
}

int main()
{
    INIT();

    struct Stats globalStats;
    setStatsZero(&globalStats);
    
    bestPotEasy3Algorithm(&globalStats, true);
    //randomMoveAlgorithm(&globalStats,false);
    mapToFile("distribution2.txt", globalStats.cm);
   
    printf("Field usage: \n");
    printMap(globalStats.cm,false);
    printf("Where is the 2048 most: \n"); //if 2048 is not the biggest anymore...
    printMap(globalStats.tf,false);

    printf("%d, %d, %d, %d \n", globalStats.choiceCount[0],globalStats.choiceCount[1],globalStats.choiceCount[2],globalStats.choiceCount[3]);
}
