#ifndef _DATA_H_
#define _DATA_H_
#include "data.c"
void countfields(int** map, struct Stats* stats);
int ppower(int base, int exponent);
void mapToFile(char* path, int** map);
int** fileToMap(char* path);
int stringToInt(char* string);
void countDirections(int m, struct Stats* stats);
void titleFingerprint(int** map, int tile, struct Stats stats);
int maxtile(int** map);
void directionLog(int modcode, struct Stats* stats);
#endif
