#Command line Two Thousand eighty four game. For AI-Training

import random as rnd
#import msvcrt
import os
import time
import matplotlib.pyplot as plt


countForAlgs = 0
countForAnalysis = 0
moveCounter = 0


def pushLeft(map, shouldPutTwos):

    for i in range(len(map)):
        rowToInsert = mergeTogether(getRow(map, i))
        map = writeRow(map, rowToInsert, i)

    if shouldPutTwos: map = putTwos(map)

    return map


def pushUp(map, shouldPutTwos):

    for i in range(len(map)):
        columnToInsert = mergeTogether(getColumn(map, i))
        map = writeColumn(map, columnToInsert, i)

    if shouldPutTwos: map = putTwos(map)

    return map


def pushRight(map, shouldPutTwos):

    for i in range(len(map)):
        rowToInsert = inverse(mergeTogether(inverse(getRow(map, i))))
        map = writeRow(map, rowToInsert, i)

    if shouldPutTwos: map = putTwos(map)

    return map


def pushDown(map, shouldPutTwos):

    for i in range(len(map)):
        columnToInsert = inverse(mergeTogether(inverse(getColumn(map, i))))
        map = writeColumn(map, columnToInsert, i)

    if shouldPutTwos: map = putTwos(map)

    return map


standardTranslator = {0: pushUp,
        1: pushLeft,
        2: pushDown,
        3: pushRight,
}

def generateMap(size):

    map = [[0 for x in range(size)] for y in range(size)]

    return map


def getRandomZero(map):

    zeroList = getZeroes(map);

    randomPick = rnd.randrange(0, len(zeroList) - 1, 2)
    randomPick = (zeroList[randomPick],zeroList[randomPick + 1])

    return randomPick


def getZeroes(map):

    list = []
    for y in range(len(map)):
        for x in range(len(map)):
            if map[x][y] == 0:
                list = list + [x,y]

    return list


def findFirstZero(row):

    zeroPos = 0
    for i in range(len(row)):
        if row[i] == 0:
           zeroPos = i;
           break

    return zeroPos


def countZeroes(row):

    counter = 0
    for o in range(len(row)):
        if row[o] == 0:
            counter = counter + 1

    return counter


def removeZerosLeft(row, all = True):

    if all == True:
        for o in range(countZeroes(row)):
            nextElement = findFirstZero(row);
            firstZero = findFirstZero(row);
            for i in range(firstZero, len(row)-1):
                if row[i] == 0:
                    nextElement = nextElement + 1

                else:
                    break

            row[firstZero] = row[nextElement]
            row[nextElement] = 0

        return row

    else:
        nextElement = findFirstZero(row);
        firstZero = findFirstZero(row);
        for i in range(firstZero, len(row)-1):
            if row[i] == 0:
                nextElement = nextElement + 1;

            else:
                break

        row[firstZero] = row[nextElement]
        row[nextElement] = 0

        return row


def inverse(row):
    copy = [0 for i in range(len(row))]

    for i in range(len(row)):
        copy[i] = row[len(row)-i-1]

    return copy


def mergeTogether(row):

    global countForAlgs
    global countForAnalysis

    for o in range(len(row)):
        for i in range(len(row) - 1):
            if row[i] == row[i + 1] and row[i] != 0:
                countForAlgs += 2*(twoToThePowerOfX(row[i]))
                countForAnalysis += 2*(twoToThePowerOfX(row[i]))
                row[i + 1] = 0
                row[i] = row[i] + 1
                break
        removeZerosLeft(row)

    return row


def putTwos(map):
    position = getRandomZero(map)

    map[position[0]][position[1]] = 1

    return map


def getRow(array, row):

    elements = [0 for o in range(len(array))]

    for i in range(len(array)):
        elements[i] = array[row][i]

    return elements


def writeRow(array, data, row):

    for i in range(len(array)):
        array[row][i] = data[i]

    return array


def getColumn(array, column):

    elements = [0 for o in range(len(array))]

    for i in range(len(array)):
        elements[i] = array[i][column]

    return elements


def writeColumn(array, data, column):

    for i in range(len(array)):
        array[i][column] = data[i]

    return array


def twoToThePowerOfX(x):
    if x == 0: return 0

    result = 2

    for i in range(x-1):
        result = result * 2

    return result


def draw(map):

    for x in range(len(map)):
        for y in range(len(map)):
            print('|', twoToThePowerOfX(map[x][y]), '|',  end='')
        print()
        print()

    print(countForAnalysis)


def prettyDraw(map):

    global countForAnalysis
    global moveCounter

    biggestNumber = 0

    for x in range(len(map)):
        for y in range(len(map)):
            if map[x][y] > biggestNumber:
                biggestNumber = map[x][y]

    lenLongestNumber = len(str(twoToThePowerOfX(biggestNumber)))
    del biggestNumber

    for x in range(len(map)):
        for y in range(len(map)):
            lenDifference = lenLongestNumber - len(str(twoToThePowerOfX(map[x][y])))

            if map[x][y] == 0:
                strNumber = " "
            else:
                strNumber = str(twoToThePowerOfX(map[x][y]))

            print('|' + (lenDifference * (" ") + strNumber),  end='')
        print('|')

    print('Score:', countForAnalysis)
    print('Move:', moveCounter)

def drawHistory(action):

    actions = []


def checkZeroes(map):

    for x in range(len(map)):
        for y in range(len(map)):
            if map[x][y] == 0:
                return True

    return False


def randomMove(map, shouldPutTwos = True):

    global standardTranslator

    randomMove = rnd.randint(0, 3)

    map = standardTranslator[randomMove](map, shouldPutTwos)

    return map


def mean(data):

    totalAmount = 0

    for i in range(len(data)):
        totalAmount += data[i]

    return totalAmount / len(data)

#Can be edited to return totalTime, bestScore or bestMap
def benchmark(games, size = 4):

    global countForAnalysis

    startTime = time.time()
    bestScore = 0
    bestMap = []

    for i in range(games):
        map = generateMap(size)
        map = putTwos(map)
        countForAnalysis = 0
        while checkZeroes(map):
            map = randomMove(map)

            map = putTwos(map)


        if countForAnalysis > bestScore:
            bestScore = countForAnalysis
            bestMap = map

    endTime = time.time()
    totalTime = endTime - startTime

    return bestScore


def resetCountForAlgs():

    global countForAlgs
    countForAlgs = 0


def resetCountForAnalysis():

    global countForAnalysis
    countForAnalysis = 0


def resetMoveCounter():

    global moveCounter
    moveCounter = 0


def lookFiveMovesAhead(map):

    options = [[[[[0 for firstMove in range(4)] for secondMove in range(4)] for thirdMove in range(4)] for fourthMove in range(4)] for fifthMove in range(4)]
    global countForAlgs
    global standardTranslator

    for firstMove in range(4):
        for secondMove in range(4):
            for thirdMove in range(4):
                for fourthMove in range(4):
                    for fifthMove in range(4):
                        mapCopy = map

                        mapCopy = standardTranslator[firstMove](mapCopy, False)
                        mapCopy = standardTranslator[secondMove](mapCopy, False)
                        mapCopy = standardTranslator[thirdMove](mapCopy, False)
                        mapCopy = standardTranslator[fourthMove](mapCopy, False)
                        mapCopy = standardTranslator[fifthMove](mapCopy, False)

                        options[firstMove][secondMove][thirdMove][fourthMove][fifthMove] = countForAlgs

                        resetCountForAlgs()


    return options


def lookThreeMovesAhead(map):

    options = [[[0 for firstMove in range(4)] for secondMove in range(4)] for thirdMove in range(4)]
    global countForAlgs
    global standardTranslator

    for firstMove in range(4):
        for secondMove in range(4):
            for thirdMove in range(4):
                mapCopy = map

                mapCopy = standardTranslator[firstMove](mapCopy, False)
                mapCopy = standardTranslator[secondMove](mapCopy, False)
                mapCopy = standardTranslator[thirdMove](mapCopy, False)


                options[firstMove][secondMove][thirdMove] = countForAlgs

                resetCountForAlgs()


    return options


def moveWithBestPotential5M(map):

    allOutComes = lookFiveMovesAhead(map)

    movesScore = [0, 0, 0, 0]

    for firstMove in range(4):
        for secondMove in range(4):
            for thirdMove in range(4):
                for fourthMove in range(4):
                    for fifthMove in range(4):
                        movesScore[firstMove] += allOutComes[firstMove][secondMove][thirdMove][fourthMove][fifthMove]

    bestMove = rnd.randint(0, 3)
    itsScore = 0

    for i in range(4):
        if movesScore[i] > itsScore:
            bestMove = i
            itsScore = movesScore[i]

    return bestMove


def moveWithBestPotential3M(map):

    allOutComes = lookThreeMovesAhead(map)

    movesScore = [0, 0, 0, 0]

    for firstMove in range(4):
        for secondMove in range(4):
            for thirdMove in range(4):
                movesScore[firstMove] += allOutComes[firstMove][secondMove][thirdMove]

    bestMove = rnd.randint(0, 3)
    itsScore = 0

    for i in range(4):
        if movesScore[i] > itsScore:
            bestMove = i
            itsScore = movesScore[i]

    return bestMove


def bestPotentialAlgorithm(size = 4, print = False, FiveMovesAhead = False):

    global standardTranslator
    global moveCounter

    map = generateMap(size)
    map = putTwos(map)
    map = putTwos(map)

    while checkZeroes(map):
        if FiveMovesAhead:
            map = standardTranslator[moveWithBestPotential5M(map)](map, False)
        else:
            map = standardTranslator[moveWithBestPotential3M(map)](map, False)

        moveCounter += 1

        map = putTwos(map)

        if print:
            os.system('clear')
            prettyDraw(map)



    return map


def circleAlgorithm(size = 4, print = False):

    global standardTranslator
    global moveCounter


    map = generateMap(size)
    map = putTwos(map)
    map = putTwos(map)

    move = 1

    while checkZeroes(map):
        whichMove = move % 4

        map = standardTranslator[whichMove](map, True)

        move += 1
        moveCounter += 1

        if print:
            os.system('clear')
            prettyDraw(map)


    return map


def randomMoveAlgorithm(size = 4, print = False):

    global moveCounter

    map = generateMap(size)
    map = putTwos(map)
    map = putTwos(map)

    while checkZeroes(map):
        map = randomMove(map, True)

        moveCounter += 1

        if print:
            os.system('clear')
            prettyDraw(map)


    return map


def compareRandomCircleBestPotential(size = 4, sampleSize = 5):

    print('Started process')

    random = []
    circle = []
    bestPotential3M = []
    bestPotential5M = []

    xAxis = []

    resetCountForAnalysis()

    global countForAnalysis

    for i in range(sampleSize):
        randomMoveAlgorithm(size)

        print('Finished randomSample', i + 1)

        random.append(countForAnalysis)
        resetCountForAnalysis()

    for i in range(sampleSize):
        circleAlgorithm(size)

        print('Finished circleSample', i + 1)

        circle.append(countForAnalysis)
        resetCountForAnalysis()

    for i in range(sampleSize):
        bestPotentialAlgorithm(size, False, False)

        print('Finished bestPotential3MSample', i + 1)

        bestPotential3M.append(countForAnalysis)
        resetCountForAnalysis()


    for i in range(sampleSize):
        bestPotentialAlgorithm(size, False, True)

        print('Finished bestPotential5MSample', i + 1)

        bestPotential5M.append(countForAnalysis)
        resetCountForAnalysis()

    for i in range(sampleSize):
        xAxis.append(i + 1)



    randomScores, = plt.plot(xAxis, random)
    circleScores, = plt.plot(xAxis, circle)
    bestPotential3MScores, = plt.plot(xAxis, bestPotential3M)
    bestPotential5MScores, = plt.plot(xAxis, bestPotential5M)
    plt.legend([randomScores, circleScores, bestPotential3MScores, bestPotential5MScores], ['Scores with randomMoves', 'Scores with circleMoves', 'Scores with bestPotential3MMoves', 'Scores with bestPotenital5MMoves'])

    plt.xlabel("Sample")
    plt.ylabel("Points")
    plt.title("Comparison of different Algorithms")

    plt.show()


def play2048(size = 4):

    global moveCounter

    map = generateMap(size)
    map = putTwos(map)
    map = putTwos(map)

    moves = {b'a': pushLeft,
            b'w': pushUp,
            b'd': pushRight,
            b's': pushDown,
    }

    while checkZeroes(map):
        os.system('clear')
        prettyDraw(map)

        try:
            map = moves[msvcrt.getch()](map, True)
        except:
            map = randomMove(map)

        moveCounter += 1

    input("You lost due to no zeroes!")

os.system('clear')

bestPotentialAlgorithm(4, True, True)
